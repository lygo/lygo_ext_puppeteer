package puppeteer

import (
	"bytes"
	"context"
	"fmt"
	"github.com/chromedp/chromedp"
	"log"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e
//----------------------------------------------------------------------------------------------------------------------

type Puppet struct {
	config    *Config
	isAppMode bool
	buffer    bytes.Buffer
	context   context.Context
	cancel    context.CancelFunc
	err       error
}

func NewPuppet(config *Config) *Puppet {
	instance := new(Puppet)
	instance.config = config
	if nil == instance.config {
		instance.config = new(Config)
	}

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *Puppet) Config() *Config {
	return instance.config
}

func (instance *Puppet) StdOut() string {
	return instance.buffer.String()
}

func (instance *Puppet) StdErr() string {
	if nil != instance.err {
		return instance.err.Error()
	}
	return ""
}

func (instance *Puppet) Open() error {
	options := instance.options()
	allocCtx, _ := chromedp.NewExecAllocator(context.Background(), options...)
	instance.context, instance.cancel = chromedp.NewContext(allocCtx, chromedp.WithLogf(log.Printf))

	go instance.run() // should pass some actions

	return nil
}

func (instance *Puppet) Close() {
	instance.cancel()
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *Puppet) options() []chromedp.ExecAllocatorOption {
	response := make([]chromedp.ExecAllocatorOption, 0)
	if instance.config.AppModeEnabled {
		instance.isAppMode = true
		if len(instance.config.StartUrl) == 0 {
			instance.config.StartUrl = "http://localhost/"
		}
		response = append(response,
			chromedp.NoFirstRun,
			chromedp.NoDefaultBrowserCheck,
			chromedp.Flag("headless", false),
			chromedp.Flag("app", instance.config.StartUrl),
		)
	} else {
		instance.isAppMode = false

	}
	return response
}

func (instance *Puppet) printf(format string, v ...interface{}) {
	instance.buffer.WriteString(fmt.Sprintf(format, v...))
}

func (instance *Puppet) run(actions ...chromedp.Action) {
	instance.err = chromedp.Run(instance.context, actions...)
}
