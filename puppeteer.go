package lygo_ext_puppeteer

import "bitbucket.org/lygo/lygo_ext_puppeteer/puppeteer"

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func RunApp(address string) (*puppeteer.Puppet, error) {
	config := new(puppeteer.Config)
	config.AppModeEnabled = true
	config.StartUrl = address
	response := puppeteer.NewPuppet(config)
	err := response.Open()
	if nil != err {
		return nil, err
	}
	return response, nil
}
