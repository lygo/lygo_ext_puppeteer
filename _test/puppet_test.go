package _test

import (
	"bitbucket.org/lygo/lygo_ext_puppeteer"
	"fmt"
	"testing"
	"time"
)

func TestPuppetApp(t *testing.T) {
	puppet, err := lygo_ext_puppeteer.RunApp("http://google.com")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	time.Sleep(7 * time.Second)
	puppet.Close()
	fmt.Println(puppet.StdOut())
	fmt.Println(puppet.StdErr())
}
