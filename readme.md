# Puppeteer #
![](icon.png)

## How to Use

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_puppeteer
```

## Dependencies ##

```
go get -u bitbucket.org/lygo/lygo_commons

go get -u github.com/chromedp/chromedp
```

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.1.0
git push origin v0.1.0
```


