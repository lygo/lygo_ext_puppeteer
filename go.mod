module bitbucket.org/lygo/lygo_ext_puppeteer

go 1.15

require (
	bitbucket.org/lygo/lygo_commons v0.1.50 // indirect
	github.com/chromedp/cdproto v0.0.0-20201204063249-be40c824ad18 // indirect
	github.com/chromedp/chromedp v0.5.4
	github.com/gobwas/httphead v0.1.0 // indirect
	golang.org/x/sys v0.0.0-20201223074533-0d417f636930 // indirect
)
